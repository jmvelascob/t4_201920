package model.logic;

public class MaxHeapCP <T extends Comparable<T>>
{
	
	private T[] arreglo;
	private int numeroElementos;
	
	public MaxHeapCP()
	{
		numeroElementos = 0;
		arreglo = (T[]) new Comparable[2];
	}
	
	public int darNumElementos()
	{
		return arreglo.length;
	}
	
	public T[] darArreglo()
	{
		return arreglo;
	}
	
	public void verificarTamaņo()
	{
		if(arreglo.length-1==numeroElementos)
		{
			T[] prov = (T[]) new Comparable[numeroElementos*2+1];
			for(int i=1;i<arreglo.length;i++)
			{
				prov[i] = arreglo[i];
			}
			arreglo = prov;
		}
	}
	
	public int posPadre(int pos)
	{
		return pos/2;
	}
	
	public int posHijoIzq(int pos)
	{
		return 2*pos;
	}
	
	public int posHijoDer(int pos)
	{
		return 2*pos+1;
	}
	
	public void agregar( T elem )
	{
		verificarTamaņo();
		arreglo[numeroElementos + 1] = elem;
		swim(numeroElementos + 1);
		numeroElementos++;
		
	}
	
	public void swim(int pos)
	{
		int posPadre = posPadre(pos);
		if(posPadre>0 && arreglo[pos].compareTo(arreglo[posPadre])>0)
		{
			exch(posPadre, pos);
			swim(posPadre);
		}
		else return;
	}
	
	public T sacarMax()
	{
		int ultimo = numeroElementos;
		exch(1,ultimo);
		T max = arreglo[ultimo];
		arreglo[ultimo] = null;
		sink(1);
		numeroElementos--;
		return max;
	}
	
	public void sink(int pos)
	{
		int posHijoIzq = posHijoIzq(pos);
		int posHijoDer = posHijoDer(pos);
		
		if( arreglo[posHijoIzq] !=null && arreglo[pos].compareTo(arreglo[posHijoIzq])<0)
		{
			exch(pos,posHijoIzq);
			sink(posHijoIzq);
		}
		else if( arreglo[posHijoDer] !=null && arreglo[pos].compareTo(arreglo[posHijoDer])<0)
		{
			exch(pos, posHijoDer);
			sink(posHijoDer);
		}
		else return;
	}
	
	public void exch(int x, int y)
	{
		T xx = arreglo[x];
		T yy = arreglo[y];
		arreglo[x] = yy;
		arreglo[y] = xx;
	}
	
	public T darMax()
	{
		return arreglo[1];
	}
	
	public boolean esVacia()
	{
		if(numeroElementos==0) return true;
		else return false;
	}
}
