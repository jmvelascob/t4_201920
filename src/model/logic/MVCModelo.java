package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import com.opencsv.CSVReader;

import model.data_structures.MaxColaCP;
import model.data_structures.Node;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	private MaxColaCP<TravelTime> cola1;
	private MaxHeapCP<TravelTime> heap;
	
	public MVCModelo()
	{
		cola1= new MaxColaCP<TravelTime>();

	}
	
	public void loadTravelTimes()
	{
		CSVReader reader = null;
		CSVReader reader2=null;
		
		try {

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));
			reader2 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-All-HourlyAggregate.csv"));


			try{
				String[] x =reader.readNext();
				String[] y =reader.readNext();
			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}

			for(String[] nextLine : reader) 
			{
				TravelTime dato = new TravelTime( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				cola1.agregar(dato);
				heap.agregar(dato);
			}
			for(String[] nextLine : reader2) 
			{
				TravelTime dato = new TravelTime( Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				cola1.agregar(dato);
				heap.agregar(dato);
			}
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null && reader2!=null ) 
			{
				try 
				{
					reader.close();
					reader2.close();

				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public MaxColaCP darCola1()
	{
		return cola1;
	}
	
	
	public MaxColaCP<TravelTime> crearMaxColaCP (int n, int hInicial, int hFinal)
	{
		MaxColaCP coli= new MaxColaCP<TravelTime>();
		int cont=0;
		while(cont<n)
		{
			TravelTime t=(TravelTime) cola1.sacarMax();
			coli.agregar(t);
			cont++;
		}
		return coli;
	}
	
	public MaxColaCP<TravelTime> generarMuestraCola(int N)
	{
		boolean esta=false;
		double rnd =   Math.random();
		MaxColaCP<TravelTime> colaz=new MaxColaCP<TravelTime>();
		Node<TravelTime> x= colaz.darPrimero();
		while(x!=null)
		{
			if(x.darData().darMean_travel_time()==rnd)
			{
				esta=true;
			}
			x=x.darSiguiente();
		}
		if (esta==true)
			generarMuestraCola(N);
		else
		{
			int cont =0;
			Node<TravelTime> y= cola1.darPrimero();
			while(cont<N)
			{
				if(y.darData().darMean_travel_time()==rnd)
				{
					colaz.agregar(y.darData());
					cont++;
				}
				y=y.darSiguiente();
			}
			
		}
		return colaz;

	}
	
	public MaxHeapCP<TravelTime> generarMuestraHeap(int N)
	{
		MaxHeapCP<TravelTime> heapProv = new MaxHeapCP<TravelTime>();
		Comparable[] arr = heap.darArreglo();
		for(int i=1; i<=N; i++)
		{
			int random = (int) (heap.darNumElementos() * Math.random());
			heapProv.agregar((TravelTime) arr[random]);
		}
		return heapProv;
	}
	
	public MaxHeapCP<TravelTime> crearMaxHeapCP(int N, int horaInicial, int horaFinal)
	{
		MaxHeapCP<TravelTime> heapProv = new MaxHeapCP<TravelTime>();
		int cont = 0;
		Comparable[] arr = heap.darArreglo();
		for(int i=1; i<heap.darNumElementos() && cont<=N; i++)
		{
			if(((TravelTime) arr[i]).darHod()>=horaInicial && ((TravelTime)arr[i]).darHod()<=horaFinal)
			{
				TravelTime x =(TravelTime) arr[i];
				heapProv.agregar(x);
				cont++;
			}
		}
		return heapProv;
	}
	
	
	


	}
	
	
	
}
