package model.data_structures;



	public class Node<T>
	{
		 T data;
		
		 Node<T> siguiente;
		
		
		public Node(T dato)
		{
			data= dato;
			siguiente=null;
		}
		
		public Node<T> darSiguiente()
		{
			return siguiente;
		}
		
		public void cambiarSig(Node<T> x)
		{
			siguiente=x;
		}
		
		public T darData()
		{
			return data;
		}
		
	}	

