package controller;

import java.util.Scanner;

import model.data_structures.MaxColaCP;
import model.data_structures.Node;

import model.logic.MVCModelo;
import model.logic.TravelTime;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";
		int ncola;
		int horaicola;
		int horafcola;
		MaxColaCP<TravelTime> cola=null;

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.loadTravelTimes();
				System.out.println("El total de viajes es: " + modelo.darCola1().darNumElementos() );
				break;

			case 2:
				System.out.println("Ingrese n�mero de viajes:");
				dato=lector.next();
				int param= Integer.parseInt(dato);
				cola= modelo.generarMuestraCola(param);
				System.out.println("Cola con "+cola.darNumElementos()+" elmentos ha sido creada.");

				break;

			case 3:
				System.out.println("Ingrese n�mero de viajes");
				ncola= Integer.parseInt(lector.next());
				System.out.println("Ingrese hora inicial:");
				horaicola=Integer.parseInt(lector.next());
				System.out.println("Ingrese hora final:");
				horafcola=Integer.parseInt(lector.next());
				long tiempoComienzo=System.currentTimeMillis();
			 	MaxColaCP<TravelTime> colaa= modelo.crearMaxColaCP(ncola, horaicola, horafcola);
				long tiempo= System.currentTimeMillis()-tiempoComienzo;
				int cont=0;
				while(cont<ncola)
				{
					System.out.println(colaa.sacarMax().darMean_travel_time());
					cont++;
				}
				System.out.println("Se demor�: "+tiempo+" milisegundos en hacer la operaci�n");
				break;
			
			case 4:
				
				System.out.println("Agregar 200000:");
				long tiempoo=System.currentTimeMillis();
				MaxColaCP<TravelTime> lol=modelo.generarMuestraCola(200000);
				long tiempoo2= System.currentTimeMillis()-tiempoo;
				System.out.println("Tiempo en agregar:"+tiempoo2);
				long t=System.currentTimeMillis();
				for(int i=0;i<lol.darNumElementos();i++)
				{
				lol.sacarMax();
				}
				long x=System.currentTimeMillis()-t;
				System.out.println("Tiempo en sacar:"+x);
			
				break;
				

			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
