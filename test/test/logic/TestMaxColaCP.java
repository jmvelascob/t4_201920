package test.logic;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import model.data_structures.MaxColaCP;
import model.logic.TravelTime;

public class TestMaxColaCP {

	private MaxColaCP<TravelTime> cola;
	
	@Before
	public void setUp()
	{
		cola= new MaxColaCP<TravelTime>();
		TravelTime uno= new TravelTime(1, 2, 2, 3.5, 4.5, 3.2, 1.1 );
		TravelTime dos= new TravelTime(28, 2, 2, 1, 4.5, 3.2, 1.1 );
		TravelTime tres= new TravelTime(36, 2, 2, 8, 4.5, 3.2, 1.1 );
		TravelTime cuatro= new TravelTime(101, 2, 2, 20, 4.5, 3.2, 1.1 );
		TravelTime cinco= new TravelTime(11, 2, 2, 18, 4.5, 3.2, 1.1 );
		//Mayor
		TravelTime seis= new TravelTime(4, 2, 2, 28, 4.5, 3.2, 1.1 );
		
		cola.agregar(uno);

		cola.agregar(dos);

		cola.agregar(tres);

		cola.agregar(cuatro);

		cola.agregar(cinco);

		cola.agregar(seis);

	}
	
	@Before
	public void setUp2()
	{
		cola= new MaxColaCP<TravelTime>();
	}

	@Test
	//CREA UNA COLA (COMIENZA VAC�A) Y LUEGO LA VAC�A
	public void agregarYVaciar()
	{
		setUp();
		assertEquals(6, cola.darNumElementos());
		cola.sacarMax();
		assertEquals(5, cola.darNumElementos());
		//Saca todos los elementos
		cola.sacarMax();
		cola.sacarMax();
		cola.sacarMax();
		cola.sacarMax();
		cola.sacarMax();
		assertEquals(0, cola.darNumElementos());
		assertEquals(true, cola.esVacia());
	}
	
	//aGREGA A UNA COLA VAC�A
	@Test
	public void vac�o()
	{
		setUp2();
		assertEquals(true, cola.esVacia());
		assertEquals(0, cola.darNumElementos());
		TravelTime x= new TravelTime(4, 2, 2, 28, 4.5, 3.2, 1.1 );
		cola.agregar(x);
		assertEquals(false, cola.esVacia());
		assertEquals(1, cola.darNumElementos());
	}
	
	//SACA EL M�XIMO DE UNA COLA

	@Test
	public void sacarMaximo()
	{
		setUp();
		assertEquals(28, cola.sacarMax().darMean_travel_time(), 0);
		assertEquals(5, cola.darNumElementos());
	}
	
	//Saca todos hasta dejarla vac�a
	@Test
	public void maxhastavacia()
	{
		setUp();
		assertEquals(28, cola.sacarMax().darMean_travel_time(), 0);
		assertEquals(20, cola.sacarMax().darMean_travel_time(),0);
		assertEquals(18, cola.sacarMax().darMean_travel_time(),0);
		assertEquals(8, cola.sacarMax().darMean_travel_time(),0);
		assertEquals(3.5, cola.sacarMax().darMean_travel_time(),0);
		assertEquals(1, cola.sacarMax().darMean_travel_time(),0);
		assertEquals(0, cola.darNumElementos());
	}
	
	
}
