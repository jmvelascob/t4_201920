package test.logic;
import model.logic.MaxHeapCP;
import model.logic.TravelTime;

import static org.junit.Assert.*;

import org.junit.Test;
public class MaxHeapCPTest 
	{
		private MaxHeapCP heap;
		
		private TravelTime e1;
		private TravelTime e2;
		private TravelTime e3;
		private TravelTime e4;
		
		public void setUpVacio()
		{
			heap = new MaxHeapCP();
		}
		
		public void setUp1()
		{
			heap = new MaxHeapCP();
			e1 = new TravelTime(1,1,1,5,1,1,1);
			e2 = new TravelTime(1,1,1,8,1,1,1);
			e3 = new TravelTime(1,1,1,3,1,1,1);
			e4 = new TravelTime(1,1,1,11,1,1,1);
			heap.agregar(e1);
			heap.agregar(e2);
			heap.agregar(e3);
			heap.agregar(e4);
		}
		
		@Test
		public void estaVaciaTest()
		{
			setUpVacio();
			assertTrue(heap.esVacia());
		}
		@Test
		public void testAgregar()
		{
			setUp1();
			assertEquals(e4, heap.darArreglo()[1] );
			assertEquals(e2, heap.darArreglo()[2] );
			assertEquals(e3, heap.darArreglo()[3] );
			assertEquals(e1, heap.darArreglo()[4] );
		}
		
		public void testSacarMax()
		{
			setUp1();
			Comparable x = heap.sacarMax();
			assertEquals(e4, x);
		}
		
	}

